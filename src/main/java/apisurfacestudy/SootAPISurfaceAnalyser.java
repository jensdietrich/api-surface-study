package apisurfacestudy;

import soot.*;
//import soot.jimple.toolkits.callgraph.CHATransformer;
import soot.jimple.toolkits.pointer.LocalMustNotAliasAnalysis;
import soot.options.*;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.util.*;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;

// java -cp target/api-surface-study-0.1.0.jar:/usr/lib/jvm/java-11-openjdk-amd64/lib/jrt-fs.jar apisurfacestudy.Main soot data/log4j-2.12.1/jars data/log4j-2.12.1/jars/log4j-api-2.12.1.jar 

public class SootAPISurfaceAnalyser {
    public static void main(String[] args) throws IOException {
	PackManager.v().getPack("wjtp").add(new Transform("wjtp.myTransform", Analyser.v()) { });
	Options.v().set_prepend_classpath(true);
	Options.v().set_verbose(true);

	List<String> pd = new ArrayList<>();
	pd.add("-process-dir");
	pd.add(args[0]);
	Options.v().set_soot_classpath(args[1]);
	Options.v().set_whole_program(true);
	soot.Main.main(pd.toArray(new String[0]));
    }

    static class Analyser extends SceneTransformer {
	private final static Analyser instance = new Analyser();
	private Analyser() {}
	public static Analyser v() { return instance; }

	@Override
	protected void internalTransform(String phaseName, Map options) {
	    System.out.println(Scene.v().getSootClassPath());
	}
    }
}

