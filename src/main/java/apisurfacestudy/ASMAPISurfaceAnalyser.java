package apisurfacestudy;

import com.google.common.base.Preconditions;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import java.io.File;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Script to analyse a project.
 * @author jens dietrich
 */
public class ASMAPISurfaceAnalyser {

    public static void main (String[] args) throws Exception {

        Preconditions.checkArgument(args.length==1,"One parameter required, name of project to analyse rel to /data");
        String projectName = args[0];
        File dataRootDir = new File("data");
        Preconditions.checkState(dataRootDir.exists());
        Preconditions.checkState(dataRootDir.isDirectory());
        File projectFolder = new File(dataRootDir,projectName);
        Preconditions.checkArgument(projectFolder.exists());
        File jarsFolder = new File(projectFolder,"jars");
        Preconditions.checkState(jarsFolder.exists());
        Preconditions.checkState(jarsFolder.isDirectory());
        File[] jars = jarsFolder.listFiles();
        Preconditions.checkState(jars.length>0);
        File moduleDef = new File(projectFolder,"module-info.java");
        Preconditions.checkState(moduleDef.exists());

        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.INFO);
        Logger LOGGER = LogManager.getLogger("log4j-analysis");

        LOGGER.info("Analysing API surface of " + projectName);

        Project project = new Project() {
            @Override
            public boolean isProjectClass(String className) {
                return className.startsWith("org.apache.logging.log4j");
            }
            @Override
            public Collection<File> getJars() {
                return Arrays.asList(jars);
            }
            @Override
            public File getModuleDef() {
                return moduleDef;
            }
        };

        Set<APISurfaceElement> publicMethods = new BytecodeAPISurfaceExtractor(EnumSet.of(BytecodeAPISurfaceExtractor.Option.METHODS, BytecodeAPISurfaceExtractor.Option.PUBLIC)).analyse(project);
        LOGGER.info("public methods: " + publicMethods.size());

        Set<String> exportedPackages = ModuleParser.getExportedPackages(project.getModuleDef());
        Set<String> exportedServices = ModuleParser.getServiceImplementations(project.getModuleDef());
        Predicate<APISurfaceElement> moduleFilter =
            m -> (
                    (m.getPackageName()!=null && exportedPackages.contains(m.getPackageName())) ||
                    (exportedServices.contains(m.getFullClassName()))

            );

        Set<APISurfaceElement> publicAndProtectedMethods = new BytecodeAPISurfaceExtractor(EnumSet.of(BytecodeAPISurfaceExtractor.Option.METHODS, BytecodeAPISurfaceExtractor.Option.PUBLIC, BytecodeAPISurfaceExtractor.Option.PROTECTED)).analyse(project);
        LOGGER.info("public and protected methods: " + publicAndProtectedMethods.size());

        Set<APISurfaceElement> exportedPublicMethods = publicMethods.stream().filter(moduleFilter).collect(Collectors.toSet());
        LOGGER.info("exported public methods: " + exportedPublicMethods.size());

        Set<APISurfaceElement> exportedPublicAndProtectedMethods = publicAndProtectedMethods.stream().filter(moduleFilter).collect(Collectors.toSet());
        LOGGER.info("exported public and protected methods: " + exportedPublicAndProtectedMethods.size());

        Set<APISurfaceElement> publicFields = new BytecodeAPISurfaceExtractor(EnumSet.of(BytecodeAPISurfaceExtractor.Option.FIELDS, BytecodeAPISurfaceExtractor.Option.PUBLIC)).analyse(project);
        LOGGER.info("public fields: " + publicFields.size());

        Set<APISurfaceElement> publicAndProtectedFields = new BytecodeAPISurfaceExtractor(EnumSet.of(BytecodeAPISurfaceExtractor.Option.FIELDS, BytecodeAPISurfaceExtractor.Option.PUBLIC, BytecodeAPISurfaceExtractor.Option.PROTECTED)).analyse(project);
        LOGGER.info("public and protected fields: " + publicAndProtectedFields.size());

        Set<APISurfaceElement> exportedPublicFields = publicFields.stream().filter(moduleFilter).collect(Collectors.toSet());
        LOGGER.info("exported public fields: " + exportedPublicFields.size());

        Set<APISurfaceElement> exportedPublicAndProtectedFields = publicAndProtectedFields.stream().filter(moduleFilter).collect(Collectors.toSet());
        LOGGER.info("exported public and protected fields: " + exportedPublicAndProtectedFields.size());

    }
}
