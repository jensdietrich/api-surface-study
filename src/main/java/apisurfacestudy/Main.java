package apisurfacestudy;

/**
 * Driver
 * @author Patrick Lam
 */
public class Main {
    public static void main (String[] args) throws Exception {
	if (args.length < 1) {
	    System.err.println("usage: asm <project-name> | soot <dir> <soot-classpath>");
	    System.exit(1);
	}
	String[] rest = new String[args.length-1];
	System.arraycopy(args, 1, rest, 0, args.length-1);
	if (args[0].equals("asm")) {
	    ASMAPISurfaceAnalyser.main(rest);
	} else if (args[0].equals("soot")) {
	    SootAPISurfaceAnalyser.main(rest);
	}
    }
}
