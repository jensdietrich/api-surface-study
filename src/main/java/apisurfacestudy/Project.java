package apisurfacestudy;

import java.io.File;
import java.util.Collection;

/**
 * Represents a project to be investigated.
 * @author jens dietrich
 */
public interface Project {

    /**
     * Whether a class with this name (fully qualified, using . as separator) is part of this project.
     * This is usually done by looking for project specific package prefixes.
     * @param className
     * @return
     */
    boolean isProjectClass (String className);

    /**
     * Get the jar(s) containing binaries.
     * @return
     */
    Collection<File> getJars() ;

    /**
     * Get the Java 9 modules definition.
     */
    File getModuleDef();

}
