package apisurfacestudy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.objectweb.asm.*;
import java.io.*;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Extracts public methods from bytecode.
 * @author jens dietrich
 */
public class BytecodeAPISurfaceExtractor implements APISurfaceExtractor {


    enum Option {
        PUBLIC,PROTECTED,SYNTHETIC,METHODS,FIELDS
    };

    private EnumSet<Option> options = null;

    public BytecodeAPISurfaceExtractor(EnumSet<Option> options) {
        this.options = options;
    }

    @Override
    public Set<APISurfaceElement> analyse(Project project) throws Exception {
        Logger LOGGER = LogManager.getLogger("bytecode-api-extractor");
        final Set<APISurfaceElement> members = new HashSet<>();

        ClassVisitor methodAnalyser = new ClassVisitor(Opcodes.ASM7) {
            String className = null;
            String packageName = null;

            @Override
            public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
                int p = name.lastIndexOf("/");
                if (p==-1) {
                    packageName = null;
                    className = name;
                }
                else {
                    packageName = name.substring(0,p).replace("/",".");
                    className = name.substring(p+1);
                }
            }
            @Override
            public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
                if (options.contains(Option.FIELDS)) {
                    boolean syntheticCheck = !checkASMFlag(access, Opcodes.ACC_SYNTHETIC) || options.contains(Option.SYNTHETIC);
                    boolean publicCheck = !checkASMFlag(access, Opcodes.ACC_PUBLIC) || options.contains(Option.PUBLIC);
                    boolean protectedCheck = !checkASMFlag(access, Opcodes.ACC_PROTECTED) || options.contains(Option.PROTECTED);
                    if (syntheticCheck && publicCheck && protectedCheck) {
                        members.add(new APISurfaceElement(packageName,className,name,descriptor));
                    }
                }
                return null;
            }

            @Override
            public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
                if (options.contains(Option.METHODS)) {
                    boolean syntheticCheck = !checkASMFlag(access, Opcodes.ACC_SYNTHETIC) || options.contains(Option.SYNTHETIC);
                    boolean publicCheck = !checkASMFlag(access, Opcodes.ACC_PUBLIC) || options.contains(Option.PUBLIC);
                    boolean protectedCheck = !checkASMFlag(access, Opcodes.ACC_PROTECTED) || options.contains(Option.PROTECTED);
                    if (syntheticCheck && publicCheck && protectedCheck) {
                        members.add(new APISurfaceElement(packageName,className,name,descriptor));
                    }
                }
                return null;
            }
        };
        for (File jar:project.getJars()) {
            try {
                LOGGER.debug("Analysing " + jar.getAbsolutePath());
                analyse(jar, methodAnalyser);
            }
            catch (IOException x) {
                LOGGER.warn("Error analysing method and fields of some class in jar file " + jar.getAbsolutePath(),x);
            }
        }

        return members;

    }


    private void analyse (File jar, ClassVisitor visitor) throws IOException {
        ZipFile archive = new ZipFile(jar);
        Enumeration<? extends ZipEntry> en = archive.entries();
        while (en.hasMoreElements()) {
            ZipEntry e = en.nextElement();
            String name = e.getName();
            if (name.endsWith(".class")) {
                try (InputStream in = archive.getInputStream(e)) {
                    analyse(in, visitor);
                }
            }
        }
    }

    private void analyse (InputStream in, ClassVisitor visitor) throws IOException {
        new ClassReader(in).accept(visitor, 0);
    }

    private boolean checkASMFlag (int flags, int opCode) {
        return (flags & opCode) == opCode;
    }
}
