package apisurfacestudy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Utility to extract information from module definitions.
 * @author jens dietrich
 */
public class ModuleParser {

    public static Set<String> getExportedPackages (File moduleSource) throws IOException  {
        return getExportedPackages(moduleSource,false);

    }
    public static Set<String> getExportedPackages (File moduleSource, boolean includeRestrictedExports) throws IOException  {
        final AtomicBoolean commentMode = new AtomicBoolean(false); // use wrapper to use inside lambda
        Set<String> packages = new HashSet<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(moduleSource))) {
            reader.lines().forEach(l -> {
               String line = l.trim();
               if (!line.startsWith("//")) {
                   if (line.startsWith("/*")) {
                       commentMode.set(true);
                   } else if (line.endsWith("*/") && commentMode.get()) {
                       commentMode.set(false);
                   }

                   if (!commentMode.get() && line.startsWith("exports")) {

                        String[] tokens = line.split("\\s+");
                        assert tokens.length>1;
                        assert tokens[0].equals("exports");

                        boolean restricted = tokens.length>3 && tokens[2].equals("to");
                        if (!restricted || includeRestrictedExports) {
                            String pck = tokens[1].replace(";", "");
                            packages.add(pck);
                        }
                   }
               }
            });
        }
        return packages;
    }

    public static Set<String> getServiceImplementations (File moduleSource) throws IOException  {
        final AtomicBoolean commentMode = new AtomicBoolean(false); // use wrapper to use inside lambda
        Set<String> classes = new HashSet<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(moduleSource))) {
            reader.lines().forEach(l -> {
                String line = l.trim();
                if (!line.startsWith("//")) {
                    if (line.startsWith("/*")) {
                        commentMode.set(true);
                    } else if (line.endsWith("*/") && commentMode.get()) {
                        commentMode.set(false);
                    }

                    // example:
                    // provides javax.json.spi.JsonProvider with org.glassfish.json.JsonProviderImpl;

                    if (!commentMode.get() && line.startsWith("provides")) {

                        String[] tokens = line.split("\\s+");
                        assert tokens.length==4; // to do check
                        assert tokens[0].equals("provides");
                        assert tokens[2].equals("with");

                        String cl = tokens[3].replace(";", "");
                        classes.add(cl);
                    }
                }
            });
        }
        return classes;
    }
}
