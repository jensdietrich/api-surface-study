package apisurfacestudy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Set;

/**
 * Simple utility to export results.
 * @author jens dietrich
 */
public class Exporter {

    public static void export (Set<APISurfaceElement> members, File result) throws Exception {

        Logger LOGGER = LogManager.getLogger("exporter");

        // export
        try (PrintWriter out = new PrintWriter(new FileWriter(result))) {
            for (APISurfaceElement member:members) {
                out.print(member.getPackageName());
                out.print('\t');
                out.print(member.getClassName());
                out.print('\t');
                out.print(member.getMemberName());
                out.print('\t');
                out.print(member.getMemberDescriptor());
                out.println();
            }
        }

        LOGGER.info("Analysis results written to " + result.getAbsolutePath());
    }
}
