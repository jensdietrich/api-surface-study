package apisurfacestudy;

import java.util.Set;

/**
 * Abstraction for analysers.
 * @author jens dietrich
 */
public interface APISurfaceExtractor {
    Set<APISurfaceElement> analyse(Project project) throws Exception ;
}
