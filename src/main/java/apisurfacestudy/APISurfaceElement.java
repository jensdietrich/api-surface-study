package apisurfacestudy;

import java.util.Objects;

/**
 * Simple representation of a class or class member.
 * @author jens dietrich
 */
public class APISurfaceElement {
    private String className = null; // local
    private String packageName = null;
    private String memberName = null; // method or field name, null if this represents just the class
    private String memberDescriptor = null; // method or field name, null if this represents just the class

    public APISurfaceElement(String packageName, String className, String memberName, String memberDescriptor) {
        this.className = className;
        this.packageName = packageName;
        this.memberName = memberName;
        this.memberDescriptor = memberDescriptor;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getFullClassName() {
        return this.packageName==null?this.className:(this.packageName+'.'+this.className);
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberDescriptor() {
        return memberDescriptor;
    }

    public void setMemberDescriptor(String memberDescriptor) {
        this.memberDescriptor = memberDescriptor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        APISurfaceElement method = (APISurfaceElement) o;
        return Objects.equals(className, method.className) &&
                Objects.equals(packageName, method.packageName) &&
                Objects.equals(memberName, method.memberName) &&
                Objects.equals(memberDescriptor, method.memberDescriptor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(className, packageName, memberName, memberDescriptor);
    }
}
