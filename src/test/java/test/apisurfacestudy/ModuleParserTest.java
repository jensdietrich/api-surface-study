package test.apisurfacestudy;

import apisurfacestudy.ModuleParser;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.*;

public class ModuleParserTest {

    private static File moduleDef = null;

    @BeforeAll
    public static void prep () throws Exception {
        URL url = ModuleParserTest.class.getResource("/moduledefs/module-info.java");
        moduleDef = new File(url.getFile());
    }

    @Test
    public void testUnrestrictedExports() throws IOException {
        Set<String> packages = ModuleParser.getExportedPackages(moduleDef,false);
        assertEquals(2,packages.size());
        assertTrue(packages.contains("com.foo.package3"));
        assertTrue(packages.contains("com.foo.package4"));
    }

    @Test
    public void testAllExports() throws IOException {
        Set<String> packages = ModuleParser.getExportedPackages(moduleDef,true);
        assertEquals(3,packages.size());
        assertTrue(packages.contains("com.foo.package3"));
        assertTrue(packages.contains("com.foo.package4"));
        assertTrue(packages.contains("com.foo.package5"));
    }

    @Test
    public void testServices() throws Exception {
        Set<String> services = ModuleParser.getServiceImplementations(moduleDef);
        assertEquals(1,services.size());
        assertTrue(services.contains("com.foo.MyFooImpl"));
    }




}
