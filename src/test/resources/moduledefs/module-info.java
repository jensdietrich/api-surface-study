/*
* // seed possible FP
* exports com.foo.package1;
*/
module org.apache.maven.plugins.jmod.it.first {
    // seed possible FP
    // exports com.foo.package2
    exports com.foo.package3;
    exports  com.foo.package4;
    exports com.foo.package5 to otherproject;
    requires myproject;
    provides com.foo.IFoo with com.foo.MyFooImpl;
}