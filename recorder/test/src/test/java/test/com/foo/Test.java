package test.com.foo;

import com.foo.Bean;
import com.foo.Provider;
import java.util.HashSet;
import java.util.Set;
import static org.junit.Assert.*;

public class Test {

    @org.junit.Test
    public void testBean() {
        Provider provider = new Provider();
        Bean bean = provider.getBean();
        bean.setField("somevalue");
        Set set = new HashSet<>();
        set.add(bean);

        assertTrue(set.contains(bean));

    }

    @org.junit.Test
    public void testProvider() {
        Provider provider = new Provider();
        provider.field1 = "foo";
        assertEquals("foo",provider.field1);

    }
}
