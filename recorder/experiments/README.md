### Experiments

This folder contains information about the experiment setup to extract used APIs for certain libraries, the respective 
modified poms, the zipped projects used, and the cached results. 

To reproduce results, unzip the respective projects, replace the `pom.xml` file, check the paths to `aspectjweaver.jar`
in the pom and adjust as necessary, and then run `mvn test`. This should produce results files
`tracked-invocations-<timestamp>.csv`.



#### org.glassfish / jsonp jsonp-1.1.2 clients

1. org.eclipse / yasson - 1.0.1 -- checked out from https://github.com/eclipse-ee4j/yasson


#### log4j log4j-2.12.1 clients

1. `spring boot` -- build system is gradle, ignore for now
2.`jboss-logging` -- plugins not found (different repo ?)pwd
3. `elasticsearch`  -- build system is gradle, ignore for now
4. `hikaricp-3.4.0` -- some tests fail, modules use different forked jvm overriding result file
5. `druid-1.1.0` --  some test fail, observed the following error in the logs: 
_The class oracle.jdbc.driver.PhysicalConnection exceeds the maximum class size supported by the JVM (constant pool too big).
6. Tried htmlunit-2.35.0 , test `com.gargoylesoftware.htmlunit.general.huge.HostParentOfWTest` did not finish after 1 h, but lots of test finished 
    ```text
    Tests run: 688, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 8.094 sec
    Running com.gargoylesoftware.htmlunit.general.ElementOuterHtmlTest
    Tests run: 576, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 3.992 sec
    Running com.gargoylesoftware.htmlunit.general.HostConstantsTest
    Tests run: 2368, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 10.086 sec
    Running com.gargoylesoftware.htmlunit.general.ElementCreationTest
    Tests run: 744, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 4.803 sec
    Running com.gargoylesoftware.htmlunit.general.ElementClosesItselfTest
    Tests run: 572, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 3.711 sec
    Running com.gargoylesoftware.htmlunit.general.ElementDefaultStyleDisplayTest
    Tests run: 1136, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 67.439 sec
    Running com.gargoylesoftware.htmlunit.general.ElementChildNodesTest
    Tests run: 572, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 153.596 sec
    ```




 