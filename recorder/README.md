## Invocation Recorder

Agent used to instrument code and record invocations of methods and access to fields (invocations for short)
in certain classes defined. 

There is an example / test in `/test`. This is not using the standard mvn / junit setup as the 
jar must be build first before running this example. 

To use the recorder in a project, manipulate the projects `pom.xml` as follows.

1. Build **this** project and install into the local repo: `mvn install`
2. Manipulate the DOM of the project to be analysed by adding a dependency to this project. Note that the version has to match the 
   version of the recorder installed in step 1, and the scope is `test`.
    ```xml
    <dependency>
      <groupId>nz.ac.vuw.ecs</groupId>
      <artifactId>api-surface-recorder</artifactId>
      <version>1.0.0</version>
      <scope>test</scope>
    </dependency>
    ```
3. Add the following configuration to the surefire plugin, or add this plugin if it does not yet exist. If the plugin exists and
already has a `<argLine>` configuration, edit this to add the respective runtime arguments. Ensure that `aspectjweaver.jar` exists at this location, or adjust path as needed.
Edit the value of the `trackpackages` property -- those are the prefixes of packages (comma-separated) for which invocations are tracked. I.e.,
all members of classes in any of those packages will be instrumened.
    ```xml
     <plugin>
       <groupId>org.apache.maven.plugins</groupId>
       <artifactId>maven-surefire-plugin</artifactId>
       <version>2.22.2</version>
       <configuration>
         <argLine>-javaagent:aspectjweaver.jar -Daj.weaving.verbose=true -Dtrackpackages="com.foo"</argLine>
       </configuration>
     </plugin> 
    ```
4. In the project under analysis, run `mvn test`. This should produce a result file(s) `tracked-invocations-<timestamp>.csv`. Since tests can be set up so that
different batches of tests are executed in separate JVMs, multiple files might be created. Those files can be merged using the 
`apisurfacestudy.recorder.MergeUtil` utility that takes the folder containg the `tracked-invocation-*.csv` files as a parameter.
5. If tests fail, use the parameters `-Dmaven.test.failure.ignore=true` and  `-Dmaven.test.error.ignore=true` -- this will miss invocations, it is recommended to capture the 
console output or the generated surefire reports to estimate the impact of this.

