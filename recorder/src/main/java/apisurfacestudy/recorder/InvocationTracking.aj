package apisurfacestudy.recorder;

import org.aspectj.lang.*;
import org.aspectj.lang.reflect.*;
import org.aspectj.lang.annotation.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Aspect to add invocation tracking.
 * @author jens dietrich
 */
public aspect InvocationTracking {

    @Before("call(* *.*(..)) && !within(java..*) && !within(apisurfacestudy..*)")
    public void trackInvocations(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        String className = method.getDeclaringClass().getName();
        if (InvocationRecorder.track(className)) {
            String methodName = method.getName();
            String descr = extractDescriptor(method);
            InvocationRecorder.recordMethodInvocation(className, methodName, descr);
        }
    }

    @Before("call(*.new(..)) && !within(java..*) && !within(apisurfacestudy..*)")
    public void trackAllocations(JoinPoint joinPoint) {
        ConstructorSignature methodSignature = (ConstructorSignature) joinPoint.getSignature();
        Constructor constructor = methodSignature.getConstructor();
        String className = constructor.getDeclaringClass().getName();
        if (InvocationRecorder.track(className)) {
            String descr = extractDescriptor(constructor);
            InvocationRecorder.recordConstructorInvocation(className, descr);
        }
    }

    @Before("get(* *.*) && !within(java..*) && !within(apisurfacestudy..*)")
    public void trackLoad(JoinPoint joinPoint) {
        FieldSignature fieldSignature = (FieldSignature) joinPoint.getSignature();
        Field field = fieldSignature.getField();
        String className = field.getDeclaringClass().getName();
        if (InvocationRecorder.track(className)) {
            InvocationRecorder.recordFieldReadAccess(className, field.getName(), field.getType().getName());
        }
    }

    @Before("set(* *.*) && !within(java..*) && !within(apisurfacestudy..*)")
    public void trackStore(JoinPoint joinPoint) {
        FieldSignature fieldSignature = (FieldSignature) joinPoint.getSignature();
        Field field = fieldSignature.getField();
        String className = field.getDeclaringClass().getName();
        if (InvocationRecorder.track(className)) {
            InvocationRecorder.recordFieldWriteAccess(className, field.getName(), field.getType().getName());
        }
    }

    private String extractDescriptor(Method method) {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        boolean f = true;
        for (Class paramType : method.getParameterTypes()) {
            if (f) f = false;
            else sb.append(',');
            sb.append(paramType.getName());
        }
        sb.append(')');
        sb.append(method.getReturnType().getName());
        return sb.toString();
    }

    private String extractDescriptor(Constructor constructor) {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        boolean f = true;
        for (Class paramType : constructor.getParameterTypes()) {
            if (f) f = false;
            else sb.append(',');
            sb.append(paramType.getName());
        }
        sb.append(')');
        return sb.toString();
    }
}