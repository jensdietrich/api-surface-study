package apisurfacestudy.recorder;

import java.io.*;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Manages the invocation recording process.
 * @author jens dietrich
 */
public class InvocationRecorder {

    private static Map<String,Integer> INVOCATION_COUNTER = new ConcurrentHashMap<>();
    private static Set<String> PACKAGE_PREFIXES = null;
    public static String PACKAGE_LIST_SYSTEM_PROPERTY = "trackpackages";
    static String INVOCATIONS_FILE_NAME = "tracked-invocations";
    static String INVOCATIONS_FILE_EXTENSION = ".csv";

    private static String CSV_SEP = "\t";

    static {
        String packagePrefixList = System.getProperty(PACKAGE_LIST_SYSTEM_PROPERTY);
        if (packagePrefixList==null) {
            throw new IllegalStateException("package prefix definition system property must be set (multiple names must be separated by comma), the property name is: " + PACKAGE_LIST_SYSTEM_PROPERTY);
        }
        PACKAGE_PREFIXES = Stream.of(packagePrefixList.split(",")).map(s -> s.trim()).collect(Collectors.toSet());

        // System.out.println("invocation-recorder: adding shutdown hook to write tracked invocations to file");
        Runtime.getRuntime().addShutdownHook(
            new Thread("invocation-recorder-shutdown-hook") {
                @Override
                public void run() {
                    super.run();
                    File invocationsFile = new File(INVOCATIONS_FILE_NAME + "-" + System.currentTimeMillis() + INVOCATIONS_FILE_EXTENSION);
                    try (PrintWriter pw = new PrintWriter(new FileWriter(invocationsFile))) {
                        String headerLine = toLine("type","class","name","descriptor","count");
                        pw.println(headerLine);
                        for (String k:INVOCATION_COUNTER.keySet()) {
                            pw.print(k);
                            pw.print(CSV_SEP);
                            pw.println(INVOCATION_COUNTER.get(k));
                        }
                    } catch (IOException x) {
                        System.err.println(("error writing invocations to file : " + invocationsFile.getAbsolutePath()));
                        x.printStackTrace();
                    }
                    System.out.println("invocation-recorder: recorded invocations written to " + invocationsFile.getAbsolutePath());
                }
            }
        );
    }

    public static boolean track(String className) {
        for (String pck:PACKAGE_PREFIXES) {
            if (className.startsWith(pck)) return true;
        }
        return false;
    }


    public static void recordFieldReadAccess(String className, String fieldName, String descriptor) {
        String line = toLine("field-read",className,fieldName,descriptor);
        INVOCATION_COUNTER.compute(line, (k,v) -> v==null ? 1 : (v+1));
    }

    public static void recordFieldWriteAccess(String className, String fieldName, String descriptor) {
        String line = toLine("field-write",className,fieldName,descriptor);
        INVOCATION_COUNTER.compute(line, (k,v) -> v==null ? 1 : (v+1));
    }

    public static void recordMethodInvocation(String className, String methodName, String descriptor) {
        String line = toLine("invocation",className,methodName,descriptor);
        INVOCATION_COUNTER.compute(line, (k,v) -> v==null ? 1 : (v+1));
    }

    public static void recordConstructorInvocation(String className, String descriptor) {
        String line = toLine("allocation",className,"<init>",descriptor);
        INVOCATION_COUNTER.compute(line, (k,v) -> v==null ? 1 : (v+1));
    }

    private static String toLine(String... values) {
        return Stream.of(values).collect(Collectors.joining(CSV_SEP));
    }
}
